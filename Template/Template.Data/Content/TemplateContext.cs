﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Template.Data.Extensions;
using Template.Data.Mappings;
using Template.Domain.Entities;

namespace Template.Data.Content
{
    public class TemplateContext :DbContext
    {
        /*criando um construtor passando DbContext*/
        public TemplateContext(DbContextOptions<TemplateContext> option)
            : base(option) { }

        #region "DBSets" 

        public DbSet<User> Users{ get; set; }

        #endregion

        /*Metoto Protected - vai dar um override na criação da conexão com o banco*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserMap());


            modelBuilder.SeedData();

            base.OnModelCreating(modelBuilder);
        }
    }
}
